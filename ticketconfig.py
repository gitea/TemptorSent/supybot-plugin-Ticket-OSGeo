###
# Copyright (c) 2013, 2014, 2015, 2016 Peter Palfrader <peter@palfrader.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

from . import tickethelpers as h
import importlib
importlib.reload(h)

class TicketConfig:
    def _setup_providers(self):
        p = []
        p.append( h.TicketHtmlTitleProvider( 'trac.osgeo.org/osgeo',
            'https://trac.osgeo.org/osgeo/ticket/',
            h.ReGroupFixup('.*?\((.*)\).*? OSGeo$'),
            prefix='OSGeo',
            postfix=' - https://trac.osgeo.org/osgeo/ticket/%s',
            status_finder = h.TracStatusExtractor
            ))

        self.providers = {}
        for i in p:
            self.providers[i.name] = i

    #addChannel(self, channel, regex=None, default=False):
    def _setup_channels(self):

        self.providers['trac.osgeo.org/osgeo'].addChannel('#osgeo*', default=True)

    def __init__(self):
        self._setup_providers()
        self._setup_channels()

# vim:set shiftwidth=4 softtabstop=4 expandtab:
