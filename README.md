## Description
This is a customize version of the Ticket plugin from https://github.com/weaselp/ticketbot.
It has been configured specifically to support the __#OSGeo\*__ IRC channels and will listen
for mentions of the form **OSGeo#_NNNN_** in those channels and print the corresponding trac
ticket title and status from the osgeo trac at **https://trac.osgeo.org/osgeo/ticket/_NNNN_**.

## Requirements
- git
- A working supybot installation built with python3. Tested with [http://doc.supybot.aperio.fr/en/latest/use/install.html Limnoria].
- A user account with shell access on the machine hosting the bot.
- Access to this git repo using your OSGeo account from the host.
- A sub-directory off the home directory named the same name as the bot's primary IRC nick, such as "OSGeoBot" -> `mkdir ~/OSGeoBot`.
- The bot must be created and properly configured, including at least the Plugins and Admin plugins. Use `cd ~/OSGeoBot && supybot-wizard`, then follow the prompts to configure.
- The bot must be running continuously on the host, you may start it from within screen or tmux using `cd ~/OSGeoBot && supybot ./OSGeoBot.conf` if you wish to be able to detach and reattach the session to view the console output, or run in the background using `cd ~/OSGeoBot && supybot ./OSGeoBot.conf 2>&1 > /dev/null &`.
- The bot must be able to connect to the IRC network, with administrative user setup and able to identify to the bot properly.
- (Optional, but highly recommended) Register the bot with the IRC network services. See [Identifying the bot to services](http://doc.supybot.aperio.fr/en/latest/use/install.html) for details.


## Installation
First, install, configure, and deploy the running bot. You should configure it to connect to **#OSGeo-test** in addition to any other __#OSGeo*__ channels you wish to provide with ticket information, channel logging, or any of the other services the bot can provide.

Once the bot is running, change into the plugins directory `cd ~OSGeoBot/plugins` and issue `git clone https://git.osgeo.org/gitea/TemptorSent/supybot-plugin-Ticket-OSGeo Ticket` to clone the customized plugin repo into the plugins directory under the name 'Ticket'.

Then, connect to the IRC network the bot is on as the administrative user and identify with the bot using `/msg OSGeoBot identify ADMINUSERNAME ADMINPASSWORD`.

Load the *Ticket* plugin by issuing `/msg OSGeoBot load Ticket`.

Done :)

## Configuration
The Ticket plugin is configured by updating the file **~/OSGeoBot/plugins/ticketconfig.py** in the repository and issuing `cd ~/OSGeoBot/plugins/Ticket && git pull` and may be rebuilt and reloaded live by issuing `/msg OSGeoBot reload Ticket`.
If you make updates on the host directly, please be sure to do `cd ~/OSGeoBot/plugins/Ticket && git commit -a -m "SHORT DESCRIPTION OF CHANGES" && git push` to avoid having losing changes.
